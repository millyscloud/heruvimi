(function ($) {
    $(document).ready(function () {
        $('#logoTop').hide();
        $('#homeNavBar').show();
        $('#doubleArrowLink').addClass('fa-4x');
    });
})(jQuery);

var btn = $('#button');

$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '300');
});

$('#doubleArrow').on('click', function(e){
    var link = "#infoSection";
    document.getElementById('doubleArrowLink').setAttribute("href", link);
});

$('#doubleArrowLink').hover(function() {
        $(this).removeClass('fa-4x');
        $(this).addClass('fa-5x');
    }, function() {
        $(this).addClass('fa-4x');
        $(this).removeClass('fa-5x');
    }
);
