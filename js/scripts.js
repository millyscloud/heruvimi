﻿

(function ($) {
    $(document).ready(function () {
        $('#logoTop').hide();
        $('#homeNavBar').hide();
        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                $('#homeNavBar').fadeIn(500);
            } else {
                $('#homeNavBar').fadeOut(500);
                $('#logoTop').fadeIn(500);
            }
        });
    });
})(jQuery);

var btn = $('#button');

$(window).scroll(function() {
    if ($(window).scrollTop() > 300) {
        btn.addClass('show');
    } else {
        btn.removeClass('show');
    }
});

btn.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({scrollTop:0}, '300');
});

